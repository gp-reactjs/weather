import axios from 'axios';

// Required variables
const API_KEY = 'ea8b0a7d075ef53fac6eac54267da5dd';
const ROOT_URL = `http://samples.openweathermap.org/data/2.5/forecast?&appid=${API_KEY}`
export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city) {
  const url = `${ROOT_URL}&q=${city},in`;
  const request = axios.get(url);
  //console.log(request);
  return {
    type: FETCH_WEATHER,
    payload: request
  };
}
